module.exports = function(api) {
  api.cache(true);
  const presets = [
    ["@babel/env", {
      targets: {
        chrome: 59
      },
      modules:false,
      "useBuiltIns":"usage"
    }]
  ];
  const plugins = ["@babel/plugin-proposal-optional-chaining","@babel/plugin-proposal-object-rest-spread"];

  return {
    presets,
    plugins
  };
}
