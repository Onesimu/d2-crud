import _forEach from 'lodash.foreach'
import _clonedeep from 'lodash.clonedeep'
import _get from 'lodash.get'
import _set from 'lodash.set'

export default {
  props: {
    /**
     * @description dialog配置
     */
    formOptions: {
      type: Object,
      default: null
    },
    /**
     * @description 表单模板
     */
    formTemplate: {
      type: Object,
      default: null
    },
    /**
     * @description 表单校验规则
     */
    formRules: {
      type: Object,
      default: null
    }
  },
  data () {
    return {
      /**
       * @description dialog显示与隐藏
       */
      isDialogShow: false,
      /**
       * @description 表单数据
       */
      formData: {},
      /**
       * @description 表单模式
       */
      formMode: 'search',
      /**
       * @description 编辑暂存数据，用于储存不在formTemplate中的数据
       */
      editDataStorage: {}
    }
  },
  methods: {
    /**
     * @description lodash.get
     */
    _get,
    /**
     * @description lodash.set
     */
    _set,
    /**
     * @description 保存行数据
     */
    handleDialogSave () {
      this.$refs.form.validate((valid) => {
        if (!valid && this.formMode != 'search') {
          return false
        }
        let rowData = {}
        if (this.formMode === 'edit') {
          rowData = _clonedeep(this.editDataStorage)
          _forEach(this.formData, (value, key) => {
            this._set(rowData, key, value)
          })
          this.$emit('row-edit', {
            index: this.editIndex,
            row: rowData
          }, () => {
            this.handleDialogSaveDone(rowData)
          })
        } else if (this.formMode === 'add') {
          _forEach(this.formData, (value, key) => {
            this._set(rowData, key, value)
          })
          this.$emit('row-add', rowData, () => {
            this.handleDialogSaveDone(rowData)
          })
        } else if (this.formMode === 'search') {
          _forEach(this.formData, (value, key) => {
            this._set(rowData, key, value)
          })
          this.$emit('row-search', rowData, () => {
            this.handleDialogSaveDone(rowData)
          })
        }
      })
    },
    /**
     * @description 取消保存行数据
     */
    handleDialogCancel (done) {
      this.$emit('dialog-cancel', done)
      this.$refs.form.resetFields()
    },
    /**
     * @description 保存完成
     */
    handleDialogSaveDone (rowData) {
      // if (this.formMode === 'edit') {
      //   this.updateRow(this.editIndex, rowData)
      //   this.editDataStorage = {}
      // } else if (this.formMode === 'add') {
      //   this.addRow(rowData)
      // }
      this.handleCloseDialog()
    },
    /**
     * @description 关闭模态框
     */
    handleCloseDialog () {
      this.isDialogShow = false
    }
  }
}
