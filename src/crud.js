import crud from './crud.vue'
import d2Crud from './d2-crud'
import d2DialogForm from './components/d2-dialog-form'
import detail from './components/detail.vue'

// import VueQuillEditor from 'vue-quill-editor'
// // require styles 引入样式
// import 'quill/dist/quill.core.css'
// import 'quill/dist/quill.snow.css'
// import 'quill/dist/quill.bubble.css'
//
// Vue.use(VueQuillEditor)

// import tinymce from 'vue-tinymce-editor'
// Vue.component('tinymce', tinymce)

if (typeof window !== 'undefined' && window.Vue) {
  Vue.component('crud', crud)
  Vue.component('d2Crud', d2Crud)
  Vue.component('d2DialogForm', d2DialogForm)
  Vue.component('detail',detail)
  Vue.prototype.$d2CrudSize = 'mini'
}

export default crud
