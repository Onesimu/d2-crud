export default {
  methods: {
     exportXls() {
        if(this.viewData.length == 0){
          const names = this.columns.map(it => '"' + it.title + '"').join()
          const s = `SELECT ${names} INTO XLSX("data.xlsx",{headers:false})`
          alasql(s)
          return
        }
        const data = alasql('SELECT * FROM HTML("div.el-table",{headers:true})')

        Array.prototype.distinct = function () {
          var ret = []
          for (var i = 0; i < this.length; i++) {
            for (var j = i + 1; j < this.length;) {
              if (this[i].id === this[j].id) {
                ret.push(this.splice(j, 1)[0])
              } else {
                j++
              }
            }
          }
          return ret
        }

        const distinct = data.distinct()
        distinct.forEach(it => delete it[''])

        const s = `SELECT * INTO XLSX("data.xlsx",{headers:true}) FROM ?`
        alasql(s, [distinct])
      },
      async importXls(evt) {
        const data = await alasql.promise('select * from xlsx(?,{headers:true})', [evt])
        // 转换列名
        data.forEach(it => {
          Object.keys(it).forEach(i => {
            const find = this.colModel.find(t => t.label == i)
            if (find) {
              const name = find.name
              it[name] = it[i]
              delete it[i]
            }
          })
          return it
        })

        if (this.formTemplate['customerId'] || this.formTemplate['companyId']) {
          const customerId = this.formTemplate['customerId'] ? 'customerId' : 'companyId'
          await antiForeignKey(data, {[customerId]: 'customers'})
        }
        if (this.$parent.foreignKeys) {
          await antiForeignKey(data, this.$parent.foreignKeys)
        }
        if (this.$parent.foreignDicts) {
          await antiForeignDict(data, this.$parent.foreignDicts)
        }
        antiDataModel(this.colModel, data)

        data.forEach(it => {
          if (this.$parent.handleBeforeCreate) {
            this.$parent.handleBeforeCreate(it)
          }
          delete it.id
          this.addDate(it)
        })
        //then的参数不一定要求是函数?
        this.entityClass.saveAll(data).then(this.success).catch(this.fail)
      }
    }
}