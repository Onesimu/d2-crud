const json2formData = (jsonData) => {
  var params = new URLSearchParams();
  for (var key in jsonData) {
    //遍历json对象的每个key/value对
    if (jsonData[key]) {
      if (key.includes("Time")) {
        const jsonDatum = jsonData[key];
        params.append("fromDate", jsonDatum[0]);
        params.append("toDate", jsonDatum[1]);
      } else {
        params.append(key, jsonData[key]);
      }
    }
  }
  return params;
}

export default {
  methods:{
    // 获取数据列表
    async getDataList(conditions = null) {
      this.dataListLoading = true;

      const params = {
        perpagenum: this.pagination.pageSize,
        topagenum: this.pagination.currentPage
      }

      const path = this.$parent.sql
      let url = '/crm/table?sql=' + path + '&' + json2formData(params)

      if(conditions){
        url += '&' + json2formData(conditions)
      }

      const res = await fetch(url, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
        credentials: 'include'
      })
      const data = await res.json()

      const colModel = data.result.fieldlist
      // colModel.forEach(it => it.type = metadata['[]'][0]['Column[]'].find(t => t.column_name == it.fieldid)?.column_type)
      this.colModel = colModel

      this.tableData = data;
      // this.colModel = data["Column[]"];
      await this.dataModel();
      this.dataList = data.result.resultlist
      this.pagination.total = data.result.allcount;
      this.reset();
      this.dataListLoading = false;
    }

  }
}
