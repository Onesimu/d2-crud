export const dataModel = async (fieldlist, dataModelMore, foreignKeys, foreignDicts, tableData, id) => {
  const columns = fieldlist.map(it => ({'title': it.column_comment, key: it.column_name,
    type: it.column_type?.replace(/\(.+\)/,'') || 'varchar', showOverflowTooltip: true,
    sortable: it.sortflag === 1 || it.type != 'varchar'}))

  const components = {
    'varchar': 'el-input',
    'Date': 'el-date-picker',
    'Time': 'el-time-picker',
    'datetime': 'el-datetime-picker',
    'Long': 'el-input-number',
    'Boolean':'checkbox'
  }

  let find = columns.find(it => it.key == 'ID')
  if (find) {
    find.width = 40
  }

  columns.forEach(it => {
    if (it.type == 'Date') {
      it.formatter = (row, column, cellValue, index) => {
        return cellValue && cellValue.replace(/\s.+/, '')
      }
      it.width = 90
    }
    if (it.type == 'Boolean') {
      it.formatter = (row, column, cellValue, index) => {
        if(cellValue === 0 || cellValue === '0'){ return '否'}
        if(cellValue == 1){ return '是'}
        return cellValue
      }
      // it.width = 90
    }
  })

  const formColumns = columns.filter(it => it.key != 'ID')

  const formTemplate = {}
  formColumns.forEach(it => formTemplate[it.key] = {
    title: it.title, component: {
      span: 8,
      name: components[it.type]
    }
  })
  // .map(it => formTemplate[it.key].component.name = components[it.type] )

  const remark = formTemplate.remarks || formTemplate.remark
  if(remark){
    remark.component.type = 'textarea'
    remark.component.span = 24
  }
  if(formTemplate.specialremark){
    formTemplate.specialremark.component.type = 'textarea'
    formTemplate.specialremark.component.span = 24
  }

  const formRules = {}
  formColumns.forEach(it => formRules[it.key] = [])

  if (foreignKeys) {
    await foreignKey(columns, formTemplate, foreignKeys, tableData)
  }
  if (foreignDicts) {
    await foreignDict(columns, formTemplate, foreignDicts, tableData)
  }
  // if (dataModelMore) {
  //   await dataModelMore(columns, formTemplate, formRules, id)
  // }
  return {columns, formTemplate, formRules}
}

const foreignDict = async function (columns, formTemplate, pairs, tableData) {
  const dict = tableData['Sys_dict[]']
  pairs.forEach(it => {
    const [attr, dictName] = it
    const filter = dict.filter(it => it.type == dictName)
    columns.find(it => it.key == attr).formatter = (row, column, cellValue, index) => {
      const find = filter.find(it => it.code == cellValue)
      return find && find.value
    }
    formTemplate[attr].component.name = 'el-select'
    formTemplate[attr].component.options = filter.map(it => ({value: Number(it.code), label: it.value}))
  })
}
const foreignKey = async function (columns, formTemplate, foreignKeys, tableData) {
  Object.keys(foreignKeys).forEach(async i => {
    // const foreignEntity = await findAll(foreignKeys[i])
    const foreignEntity = tableData['[]'].map(it => it[foreignKeys[i]])
    columns.find(it => it.key == i).formatter = (row, column, cellValue, index) => {
      const find = foreignEntity.find(it => (it.ID) == cellValue)
      if (!find) {return cellValue}
      const name = find.name || find.ShortName
      return name
    }
    formTemplate[i].component.name = 'el-select'
    formTemplate[i].component.options = foreignEntity.map(it => {
      const name = it.name || it.ShortName
      const id = it.ID
      return {value: id, label: name}
    })
  })

}

export const antiForeignKey = async function (data, foreignKeys) {
  Object.keys(foreignKeys).forEach(async i => {
    const foreignEntity = await findAll(foreignKeys[i])
    data.forEach(it => {
      const find = foreignEntity.find(t =>
        (t.name || t.zhName || t.nameCn || t.username) == it[i])
      if (find) {
        const id = find.id || find.deptId || find.userId
        it[i] = id
      }
    })
  })

}

export const antiForeignDict = async function (data, pairs) {
  const dict = await findAll('sysDicts')
  pairs.forEach(it => {
    const [attr, dictName] = it
    const filter = dict.filter(it => it.type == dictName)
    data.forEach(t => {
        const find = filter.find(it => it.value == t[attr])
        if (find) {
          t[attr] = find.code
        }
      }
    )
  })
}

export const antiDataModel = (colModel, data) => {
  colModel.forEach(it => {
    if (it.type == 'Date') {
      data.forEach(i => i[it.name] = i[it.name] ? i[it.name] + ' 00:00:00': i[it.name])
    }
  })
}
