export default {
  methods:{
    // 获取数据列表
    async getDataList(conditions = {}) {
      this.dataListLoading = true;

      let others = ``;
      let fks = "";
      if (this.$parent.foreignKeys) {
        const foreignKeys = this.$parent.foreignKeys;
        fks = Object.keys(foreignKeys)
        .map(
          it =>
            `
        "${foreignKeys[it]}": {
            "ID@": "/${this.entityName}/${it}"
        }`
        )
        .join();
        // console.log(fks)
      }
      if (this.$parent.foreignDicts) {
        const types = this.$parent.foreignDicts.map(it => it[1]);
        const str = `"Sys_dict[]": {
               "count": 0,
        "Sys_dict": {
            "type{}": ${JSON.stringify(types)}
        }
    },`;
        others += str;
      }
      const data = await getArray(
        this.entityName,
        conditions,
        "," + fks,
        others,
        this.pagination.pageSize,
        this.pagination.currentPage - 1
      );

      this.tableData = data;
      this.colModel = data["Column[]"];
      await this.dataModel();
      this.dataList = data["[]"].map(it => it[this.entityName]);
      this.pagination.total = data.total;
      this.reset();
      this.dataListLoading = false;
      // const data =await this.findData(param)
      // this.totalPage = this.viewData.page.totalElements
    },
  }
}
