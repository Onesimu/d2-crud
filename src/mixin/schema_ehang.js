export const dataModel = async (fieldlist, dataModelMore, foreignKeys, foreignDicts, id) => {
  const columns = fieldlist.map(it => ({'title': it.fieldname, key: it.fieldid,
    type: it.type?.replace(/\(.+\)/,'') || 'varchar', showOverflowTooltip: true,
    sortable: it.sortflag === 1 || it.type != 'varchar'}))

  const components = {
    'varchar': 'el-input',
    'Date': 'el-date-picker',
    'Time': 'el-time-picker',
    'datetime': 'el-datetime-picker',
    'Long': 'el-input-number',
    'Boolean':'checkbox'
  }

  // columns.forEach(it => {
  //   if (it.type == 'Date') {
  //     it.formatter = (row, column, cellValue, index) => {
  //       return cellValue && cellValue.replace(/\s.+/, '')
  //     }
  //     it.width = 90
  //   }
  //   if (it.type == 'Boolean') {
  //     it.formatter = (row, column, cellValue, index) => {
  //       if(cellValue === 0 || cellValue === '0'){ return '否'}
  //       if(cellValue == 1){ return '是'}
  //       return cellValue
  //     }
  //     // it.width = 90
  //   }
  // })

  const formColumns = columns.filter(it => it.key != 'id')

  const formTemplate = {}
  formColumns.forEach(it => formTemplate[it.key] = {
    title: it.title, component: {
      span: 6,
      name: components[it.type]
    }
  })
  // .map(it => formTemplate[it.key].component.name = components[it.type] )

  // const remark = formTemplate.remarks || formTemplate.remark
  // if(remark){
  //   remark.component.type = 'textarea'
  //   remark.component.span = 24
  // }
  // if(formTemplate.specialremark){
  //   formTemplate.specialremark.component.type = 'textarea'
  //   formTemplate.specialremark.component.span = 24
  // }

  const formRules = {}
  formColumns.forEach(it => formRules[it.key] = [])

  if (foreignKeys) {
    await foreignKey(columns, formTemplate, foreignKeys)
  }
  if (foreignDicts) {
    await foreignDict(columns, formTemplate, foreignDicts)
  }
  if (dataModelMore) {
    await dataModelMore(columns, formTemplate, formRules, id)
  }
  return {columns, formTemplate, formRules}
}

const findAll = async (entityName) => {
  const name = entityName.endsWith('ies') ? entityName.substring(0, entityName.length - 3) + 'y'
    : entityName.substring(0, entityName.length - 1)
  const tableName = name.replace(/([A-Z])/g, '_$1').toLowerCase()
  // 对于按驼峰命名的表不转换列名大小写
const capNames = ['customer']
  const data = alasql(`SELECT * FROM ${tableName}`)

  if (data && data[0]) {
    if(capNames.includes(tableName)){
      return data
    }
    const columns = alasql(`SHOW COLUMNS FROM ${tableName}`)
    const selectAs = columns.map(it => `[${it.columnid}] as [${toHump(it.columnid)}]`).join(',')
    return alasql(`SELECT ${selectAs} FROM ${tableName}`)
  } else {
    const param = {
      'page': 0,
      'size': 30000
      // 'key': this.dataForm.key
    }
    const serverData = await spring.extend(entityName).findAll(param)
    if(!serverData || serverData.length == 0){return}
    const data = serverData.map(it => it.data())
    if(capNames.includes(tableName)){
      const s1 = `SELECT * INTO ${tableName} FROM ?`
      alasql(s1, [data])
      return data
    }
    // 无数据时应判断
    const keys = Object.keys(data[0])
    // 字符串数组转为字符串时自动join,默认以,分割
    const as = keys.map(it => `[${it}] as [${toLine(it)}]`)
    // const as = columns.map(it => `[${toHump(it.columnid)}] as [${it.columnid}]`).join(',')
    const s1 = `SELECT ${as} INTO ${tableName} FROM ?`
    alasql(s1, [data])
    return data
  }
}
const foreignDict = async function (columns, formTemplate, pairs) {
  const dict = await findAll('sysDicts')
  pairs.forEach(it => {
    const [attr, dictName] = it
    const filter = dict.filter(it => it.type == dictName)
    columns.find(it => it.key == attr).formatter = (row, column, cellValue, index) => {
      const find = filter.find(it => it.code == cellValue)
      return find && find.value
    }
    formTemplate[attr].component.name = 'el-select'
    formTemplate[attr].component.options = filter.map(it => ({value: Number(it.code), label: it.value}))
  })
}
const foreignKey = async function (columns, formTemplate, foreignKeys) {
  Object.keys(foreignKeys).forEach(async i => {
    const foreignEntity = await findAll(foreignKeys[i])
    columns.find(it => it.key == i).formatter = (row, column, cellValue, index) => {
      const find = foreignEntity.find(it => (it.id || it.deptId || it.userId) == cellValue)
      if (!find) {return cellValue}
      const name = find.name || find.zhName || find.nameCn || find.username
      return name
    }
    formTemplate[i].component.name = 'el-select'
    formTemplate[i].component.options = foreignEntity.map(it => {
      const name = it.name || it.zhName || it.nameCn || it.username
      const id = it.id || it.deptId || it.userId
      return {value: id, label: name}
    })
  })

}

export const antiForeignKey = async function (data, foreignKeys) {
  Object.keys(foreignKeys).forEach(async i => {
    const foreignEntity = await findAll(foreignKeys[i])
    data.forEach(it => {
      const find = foreignEntity.find(t =>
        (t.name || t.zhName || t.nameCn || t.username) == it[i])
      if (find) {
        const id = find.id || find.deptId || find.userId
        it[i] = id
      }
    })
  })

}

export const antiForeignDict = async function (data, pairs) {
  const dict = await findAll('sysDicts')
  pairs.forEach(it => {
    const [attr, dictName] = it
    const filter = dict.filter(it => it.type == dictName)
    data.forEach(t => {
        const find = filter.find(it => it.value == t[attr])
        if (find) {
          t[attr] = find.code
        }
      }
    )
  })
}

export const antiDataModel = (colModel, data) => {
  colModel.forEach(it => {
    if (it.type == 'Date') {
      data.forEach(i => i[it.name] = i[it.name] ? i[it.name] + ' 00:00:00': i[it.name])
    }
  })
}
