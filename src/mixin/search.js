import _forEach from 'lodash.foreach'
import _clonedeep from 'lodash.clonedeep'

export default {
  props: {
    /**
     * @description 是否显示新增按钮
     */
    seachMode: {
      type: Boolean,
      default: false
    },
  },
  methods: {
    /**
     * @description 新增行数据
     */
    handleSearch () {
      this.formMode = 'search'
      this.isDialogShow = true
      this.formData = this.formTemplate ? _clonedeep(this.formTemplate) : {}
      _forEach(this.formData, (value, key) => {
        this.formData[key] = this.formTemplate[key].value
      })
    }
  }
}
