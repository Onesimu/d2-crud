function getDate() {
  const date = new Date()
  date.setHours(date.getHours() + 8)
  return date.toJSON().replace(/\..+/, '').replace('T', ' ')
}

export default {
  data: {
    current: {},
    entityClass: {},
    user: {},
  },
  async beforeMount() {
    // this.getUser()
  },
  methods: {
    getUser() {
      const user = alasql('SELECT * from user')
      if(user && user[0]){
        this.user = user[0]
      } else{
        $.getJSON(baseURL + 'sys/user/info?_' + $.now(), (r) => {
          this.user = r.user
          alasql('SELECT * INTO user FROM ?', [[r.user]]);
        })
      }
    },
    addDate: function (current = this.current) {
      // current.updateDate = getDate()
      // current.updateBy = this.user.username
			current.updatetime = getDate()
			current.updatestaffid = this.user.username
      if (!current.id) {
        // current.createDate = getDate()
        // current.createBy = this.user.username
				current.createtime = getDate()
				current.createstaffid = this.user.username
      }
    },
    success(json, done = () => {}) {
      if(json && json.code != 200){
        alert(json.msg)
        return
      }
      // this.$notify({
      //   title: '消息',
      //   message: '操作成功',
      //   duration: 0
      // });
      alert('操作成功')
        this.reload()
        done()
    },
    fail(err) {
      console.log(err)
      alert(err)
    },
    async getInfo(id) {
      const one = await getObject(this.entityName, id)
      this.current = one[this.entityName]
    },
    reload(event) {
      // if(this.getDataList){
      //   this.getDataList()
      // }
      $('#formpreordergrid').submit();
    }
  }
}
